/**
 * @file   MyPythia.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 19 09:05:07 2019
 * @copy   2019 Christian Holm Christensen
 * @brief  Wrapper of running Pythia 
 */
#ifndef MYPYTHIA_HH
#define MYPYTHIA_HH
#include "Progress.hh"
#include "Option.hh"
#include <Pythia8/Pythia.h>
#include <Pythia8/HeavyIons.h>
#include <Pythia8Plugins/HepMC2.h>
#include <random>

/** 
 * Wrapper around running Pythia. 
 */
struct MyPythia
{
  CommandLine            _cl;
  Option<bool>           _help;
  Option<unsigned long>  _nev;
  Option<int>            _proj;
  Option<int>            _targ;
  Option<double>         _sqrtS;
  Option<std::string>    _data;
  Option<bool>           _verb;
  Option<unsigned long>  _seed;
  Option<std::string>    _cmd;
  Option<unsigned short> _tries;
  Option<unsigned>       _freq;

  /**
   * Constructor 
   */
  MyPythia()
    : _cl(),
      _help ('h',"help",      "Show the help",              false),
      _nev  ('n',"events",    "Number of events",           "EVENTS", 10),
      _proj ('p',"projectile","Projectile beam",            "PDG",    2212),
      _targ ('t',"target",    "Target beam",                "PDG",    2212),
      _sqrtS('e',"energy",    "Collision energy",           "GEV",    5023),
      _data ('d',"data",      "Data directory",             "DIR",    ""),
      _verb ('v',"verbose",   "Be verbose",                 false),
      _seed ('s',"seed",      "Random number seed",         "NUMBER", 0),
      _cmd  ('c',"command",   "Command files",              "FILES",  {}),
      _tries('T',"tries",     "Maximum event tries",        "N",      10),
      _freq ('f',"progress",  "Progress frequency",         "EVENTS", 0)
  {
    _cl.add(_help);
    _cl.add(_nev);
    _cl.add(_proj);
    _cl.add(_targ);
    _cl.add(_sqrtS);
    _cl.add(_data);
    _cl.add(_verb);
    _cl.add(_seed);
    _cl.add(_tries);
    _cl.add(_cmd);
    _cl.add(_freq);
  }
  /** 
   * Parse command line 
   * 
   * @param argc  Number of arguments 
   * @param argv  Vector of arguments 
   * @param out   Possible output stream for messages 
   * 
   * @return true on success 
   */
  bool parse(int argc, char** argv, std::ostream& out=std::cout)
  {
    if (!_cl.parse(argc, argv) || _help.value()) {
      _cl.usage(argv[0], std::cout);
      return false;
    }
    if (_verb.value())
      _cl.print(std::cout);
    return true;
  }
  /** 
   * Initialize the Pythia EG.  Derived classes can overload this to
   * set-up specific things (e.g., Angantyr).
   * 
   * @param pythia Pythia object
   */
  virtual void init(Pythia8::Pythia& pythia)
  {
    // Setup the beams.
    pythia.readString("Beams:idA = " + _proj .toString());
    pythia.readString("Beams:idB = " + _targ .toString());
    pythia.readString("Beams:eCM = " + _sqrtS.toString());
    pythia.readString("Beams:frameType = 1");

    // Seed
    unsigned int seed = _seed.value();
    if (seed == 0)  {
      // Sample machine entropy for seed
      std::random_device rd;
      seed = rd() & 0x7FFFFFFF;
    }
    pythia.readString("Random:setSeed = on");
    pythia.readString("Random:seed = " + std::to_string(seed));

    // Output 
    std::string q = _verb.value() ? "off" : "on";
    std::string v = _verb.value() ? "on"  : "off";
    pythia.readString("Print:quiet = " + q);
    pythia.readString("Stat:showProcessLevel = " + v);

    // Configuration
    for (auto c : _cmd.values())  {
      if (_verb.value())
	std::cout << "Read settings from " << c << std::endl;
      pythia.readFile(c);
    }
  }
  /** 
   * Generate a single event.  Try for as many times as needed or no
   * more than the number of times specified on the command line.
   * 
   * @param pythia Pythia object 
   * 
   * @return true on success
   */
  virtual bool generate(Pythia8::Pythia& pythia)
  {
    short test = _tries.value();
    while (!pythia.next() && test--) {}
    return test >= 0;
  }
#if PYTHIA_VERSION_INTEGER >= 8240
  using NucleonList = std::set<const Pythia8::Nucleon*>;
  virtual std::tuple<bool,double,double,double>
  sumHarmonic(const Pythia8::Nucleon* nucl,
	      NucleonList& seen,
	      size_t order)
  {
    if (seen.find(nucl) != seen.end()) return std::make_tuple(false,0.,0.,0.);
    
    seen.insert(nucl);
    const Pythia8::Vec4& v = nucl->bPos();
    double x = v.px();
    double y = v.py();
    double r = sqrt(x*x + y*y);
    double p = atan2(y,x);
    double q = pow(r,order);
    return std::make_tuple(true,q*cos(order*p),q*sin(order*p),q);
  }
#endif 
  virtual std::pair<double,double> harmonics(const Pythia8::HIInfo& phi)
  {
#if PYTHIA_VERSION_INTEGER < 8240
    return std::make_pair(0.,0.);
#else 
    auto subCols = const_cast<Pythia8::HIInfo&>(phi).subCollisionsPtr();
    NucleonList seenProj;
    NucleonList seenTarg;
    double cphi  = 0;
    double sphi  = 0;
    double rn    = 0;
    size_t np    = 0;
    size_t order = 1;
    for (auto sc : *subCols) {
      auto psum = sumHarmonic(sc.proj, seenProj, order);
      if (std::get<0>(psum)) {
	cphi += std::get<1>(psum);
	sphi += std::get<2>(psum);
	rn   += std::get<3>(psum);
	np++;
      }

      auto tsum = sumHarmonic(sc.proj, seenProj, order);
      if (std::get<0>(tsum)) {
	cphi += std::get<1>(tsum);
	sphi += std::get<2>(tsum);
	rn   += std::get<3>(tsum);
	np++;
      }
    }
    cphi /= np;
    sphi /= np;
    rn   /= np;
    double psi  =  (atan2(sphi, cphi) + M_PI) / order;
    double ecc  =  sqrt(sphi*sphi + cphi*cphi) / rn;

    return std::make_pair(psi,ecc);
#endif 
  }
  virtual void takeHeavyIon(Pythia8::Pythia& pythia,
			    HepMC::GenEvent& hepMcEvent)
  {
    if (!pythia.info.hiinfo) return;

    
    // Calculate number of participants (absorptively and diffractively)
    const Pythia8::HIInfo& phi = *pythia.info.hiinfo;
    int nCollHard = phi.nCollND();
    int nColl     = phi.nCollTot();
    int nPartTarg = phi.nPartTarg();
    int nPartProj = phi.nPartProj();
    int nCollNW   = phi.nCollTot()-phi.nCollSDP();
    int nCollWN   = phi.nCollTot()-phi.nCollSDT();
    int nCollWW   = phi.nCollNDTot();
    int nSpecNeu  = 0;
    int nSpecProt = 0;
    float b       = phi.b();
    float sigma   = phi.sigmaTot();

    auto psiecc = harmonics(phi);
    float psi     = psiecc.first;
    float ecc     = psiecc.second;
      
      
    HepMC::HeavyIon hi;
    hi.set_Ncoll_hard		       (nCollHard);// # hard scatterings
    hi.set_Npart_proj		       (nPartProj);// # part in projectile
    hi.set_Npart_targ		       (nPartTarg);// # part in target
    hi.set_Ncoll		       (nColl);    // # N-N collisions  
    hi.set_spectator_neutrons	       (nSpecNeu); // # spectator neutrons
    hi.set_spectator_protons	       (nSpecProt);// # spectator protons
    hi.set_N_Nwounded_collisions       (nCollNW);  // # N-wound coll
    hi.set_Nwounded_N_collisions       (nCollWN);  // # wound-N coll
    hi.set_Nwounded_Nwounded_collisions(nCollWW);  // # wound-wound coll
    hi.set_impact_parameter	       (b);	   // Imp. Par. in fm
    hi.set_event_plane_angle	       (psi);      // azimuthal angle
    hi.set_eccentricity	               (ecc);      // Eccentricity of part
    hi.set_sigma_inel_NN	       (sigma);    // NN INEL X-section
    // hi.set_centrality	       (0);	       // Percent

    // This allocates a copy of the passed object 
    hepMcEvent.set_heavy_ion(hi);
  }
			    
  /** 
   * Loop over number of events 
   * 
   * @param pythia Pythia object 
   * @param out    The output stream 
   * 
   * @return true on sucess 
   */
  virtual bool loop(Pythia8::Pythia& pythia, std::ostream& out=std::cout)
  {
    HepMC::Pythia8ToHepMC toHepMc;
    HepMC::GenEvent       hepMcEvent;
    HepMC::HeavyIon       hi;
    HepMC::IO_GenEvent    hepMcOut(out);  

    if (!pythia.init()) return false;
    
    // Loop over events.
    // double sumw = 0;
    Progress progress(_freq.value());
    unsigned long nev = _nev.value();
    unsigned long iev = 0;
    for (; iev < nev; ++iev ) {
      if (!generate(pythia)) break;

      hepMcEvent.clear();
      toHepMc.fill_next_event(pythia, &hepMcEvent);
      takeHeavyIon(pythia, hepMcEvent);

      const HepMC::GenEvent* outEvent = &hepMcEvent;
      hepMcOut << outEvent;
      progress.print(iev+1, nev);
    }
    // Print statistics
    progress.print(iev+1,nev,"Finished");
    progress.end(nev);
    return true;
  }
  /** 
   * Run the model 
   * 
   * @param argc Number of arguments 
   * @param argv Vector of arguments 
   * @param out  Possible redirected output stream 
   * 
   * @return status 
   */
  int run(int argc, char** argv, std::ostream& out=std::cout)
  {
    if (!parse(argc, argv)) return 1;

    // Generator
    Pythia8::Pythia pythia(_data.value(),false);
    init(pythia);
    loop(pythia,out);

    if (_verb.value()) pythia.stat();

    return 0;
  }
};
#endif
//
// EOF
//
