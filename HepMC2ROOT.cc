/**
 * @file   HepMC2ROOT.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 15 14:00:12 2019
 * @brief  HepMC2ROOT via Rivet 
 * @copy   2019 Christian Hom Christensen
 * 
 * Build as 
 *
 *   rivet-buildplugin -r RivetHepMC2ROOT.so HepMC2ROOT.cc
 */
#include <Rivet/Analysis.hh>
#include "TreeOutput.hh"

namespace Rivet
{
  /**
   * An analysis that stores all particles on a ROOT TTree in a TFile. 
   */
  class HepMC2ROOT : public Analysis, Tools::TreeOutput
  {
  public:
    /** 
     * Constructor
     */
    HepMC2ROOT()
      : Analysis("HepMC2ROOT"), TreeOutput(),
	_unique(true), _reorder(true)
    {
      buildInfo();      
    }
    /** 
     * Initialize this analysis.  This simply sets up the ROOT tree
     * output.  A more suffisticated analysis can declare projections
     * to be used for selectively filling the output tree.
     *
     * Note, after calling `setupTree` a class is free to add more
     * branches to the output tree should that be needed.  E.g., one
     * could fill projection values (such as trigger, centrality, ...)
     * into separate branches.
     * 
     */
    void init()
    { 
      std::string outname = getOption("output",std::string("out.root"));
      _unique  = getOption("unique", _unique);
      _reorder = getOption("reorder",_reorder);      

      setupTree(outname);
    }
    /** 
     * Analyse a single event. In this class, we simply store _all_
     * particles on the output tree.  A more sophisticated analysis
     * can use projections to select which particles to output.  Also,
     * for speed optimisation one should probably select which if the
     * schemes
     *
     * - fillTreeUniqueParticles vs. fillTreeParticles 
     * - fillTreeReordered vs fillTree 
     *
     * one want's to use.   
     *
     * If one has added more branches in `init` one should take care
     * to properly reset and fill the stored data of those branches
     * here.

     * @param e Event
     */
    void analyze(const Event& e)
    {
      fillTreeHeader(e);
      if (_unique) fillTreeUniqueParticles(e.allParticles());
      else         fillTreeParticles(e.allParticles());
      if (_reorder) fillTreeReordered();
      else          fillTree();
    }
    /** 
     * Finalize this analysis.  This flushes the tree to disk. 
     * 
     */
    void finalize() 
    {
      closeTree();
      std::string src = getOption("source");
      if (src.empty()) return;

      std::ofstream out(src.c_str());
      TreeOutput::saveSource(out);
      out.close();
    }
  protected:
    /** 
     * @{ 
     * @name Some options 
     */
    bool _unique;
    bool _reorder;
    /* @} */
    void buildInfo()
    {
      info().setName("HepMC2ROOT");
      info().setAuthors({"Christian Holm Christensen"});
      info().setSummary("Write out ROOT TTree of particles");
      info().setDescription("Write ROOT TTree to output file");
      info().setRunInfo("Nothing");
      info().setBeams({std::make_pair(0,0)});
      info().setExperiment("ANY");
      info().setCollider("ANY");
      info().setYear("2019");
      info().setStatus("VALIDATED");
      info().setOptions({"unique=true,false",
			 "reorder=true,false",
			 "output=*",
			 "source=*"});
      info().setNeedsCrossSection(false);
      info().setReentrant(false);
    }

  };
  DECLARE_RIVET_PLUGIN(HepMC2ROOT);
}
//
// EOF
//


