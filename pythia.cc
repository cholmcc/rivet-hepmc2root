/**
 * @file   pythia.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 19 08:56:29 2019
 * @copy   2019 Christian Holm Christensen
 * @brief  Generate pp collisions with Pythia
 * 
 * To build, do 
 *
 @code 
   g++ -g pythia.cc \
     `pythia8-config --cxxflags` \
     -L`pythia8-config --libdir` \
     -lpythia8 -lHepMC -ldl -o pythia 
 @endcode 
 *
 * To run, do 
 *
 @code 
 ./pythia \
   [-n EVENTS] \
   [-p PROJECTILE_PDG] \
   [-t TARGET_PDG] \
   [-e COLLISION_ENERGY_IN_GEV] \
   [-d DATA_DIRECTORY] \
   [-s RANDOM_NUMBER_SEED] \
   [-T MAXIMUM_ATTEMPTS_AT_EVENT_GENERATION] \
   [-c SETTINGS_FILE] \
   [-S] \
   [-v]
 @endcode 
 *
 * For more, do 
 *
 @code 
 ./pythia -h
 @endcode 
 */

#include "MyPythia.hh"

struct PPPythia : public MyPythia
{
  Option<bool> _soft;
  
  PPPythia()
    : MyPythia(),
      _soft('S',"soft","All soft QCD", true)
  {
    _cl.add(_soft);
  }
  void init(Pythia8::Pythia& pythia)
  {
    MyPythia::init(pythia);
    std::string s = (_soft.value() ? "on" : "off");
    pythia.readString("SoftQCD:all = " + s);
  }
};

    
//__________________________________________________________________
/** 
 * Program 
 * 
 * @param argc Number of arguments
 * @param argv Vector of arguments 
 *  
 * @return return code 
 */
int main(int argc, char** argv)
{
  /** Copy of standard out and redirect std::cout to std::clog */
  std::ostream cout(std::cout.rdbuf(std::clog.rdbuf()));
  std::cin.tie(0); // Not tied to anything
  cout.tie(0);

  PPPythia pythia;

  return pythia.run(argc, argv, cout);
}
//
// EOF
//
