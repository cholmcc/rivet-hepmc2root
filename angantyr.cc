/**
 * @file   angantyr.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 19 08:56:41 2019
 * @copy   2019 Christian Holm Christensen
 * @brief  Generate A-A collisions with Angantyr 
 * 
 * To build, do 
 *
 @code 

   g++ -g angantyr.cc \
     `pythia8-config --cxxflags` \
     -L`pythia8-config --libdir` \
     -lpythia8 -lHepMC -ldl -o angantyr 

 @endcode 
 *
 * To run, do 
 *
 @code 
 ./angantyr \
   [-n EVENTS] \
   [-p PROJECTILE_PDG] \
   [-t TARGET_PDG] \
   [-m HEAVY_ION_MODEL] \
   [-e COLLISION_ENERGY_IN_GEV] \
   [-d DATA_DIRECTORY] \
   [-s RANDOM_NUMBER_SEED] \
   [-T MAXIMUM_ATTEMPTS_AT_EVENT_GENERATION] \
   [-c SETTINGS_FILE] \
   [-S] \
   [-v]
 @endcode 
 *
 * For more, do 
 *
 @code 
 ./angantyr -h
 @endcode 
 */
#include "MyPythia.hh"

#define A_PDG(Z,A) \
  (1000000000 + /* ION identifier   */		    \
   0*10000000 + /* # strange quarks */		    \
      Z*10000 + /* atomic number    */		    \
         A*10 + /* atomic weight    */		    \
          0*1)  /* Isomer number    */  
#define PB_PDG A_PDG(82,208)
#define XE_PDG A_PDG(54,129)


struct Angantyr : public MyPythia
{
  Option<unsigned short> _model;
  Option<unsigned short> _fits;

  Angantyr()
    : MyPythia(),
      _model('m',"model",     "0:fixed,1:random,2:opacity", "MODEL",  0),
      _fits ('f',"fits",      "X-section fit iterations",   "N",      20)      
  {
    _cl.add(_model);
    _cl.add(_fits);
    _proj.set(std::to_string(PB_PDG));
    _targ.set(std::to_string(PB_PDG));
  }
  void init(Pythia8::Pythia& pythia)
  {
    // Call base class 
    MyPythia::init(pythia);
    
    // Define ions
    pythia.particleData.addParticle(PB_PDG, "PB");
    pythia.particleData.addParticle(XE_PDG, "XE");
    // Set full Heavy-ion mode
    pythia.readString("Angantyr:CollisionModel = " + _model.toString());
    // Initialize the Angantyr model 
    pythia.readString("HeavyIon:SigFitErr = "
		      "0.02,0.02,0.1,0.05,0.05,0.0,0.1,0.0");
    pythia.readString("HeavyIon:SigFitDefPar = "
		      "17.49,2.01,0.33,0.0,0.0,0.0,0.0,0.0");
    pythia.readString("HeavyIon:SigFitNGen = " + _fits.toString());
  }
};
//__________________________________________________________________
/** 
 * Program 
 * 
 * @param argc Number of arguments
 * @param argv Vector of arguments 
 *  
 * @return return code 
 */
int main(int argc, char** argv)
{
  /** Copy of standard out and redirect std::cout to std::clog */
  std::ostream cout(std::cout.rdbuf(std::clog.rdbuf()));
  std::cin.tie(0); // Not tied to anything
  cout.tie(0);

  Angantyr angantyr;

  return angantyr.run(argc, argv, cout);
}
//
// EOF
//
