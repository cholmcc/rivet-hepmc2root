#
#
#
PACKAGE		= myeg
VERSION		= 0.1
DISTDIR		= $(PACKAGE)-$(VERSION)

PYTHIA_CONFIG	= pythia8-config 
PYTHIA_CXXFLAGS	= $(shell pythia8-config --cxxflags)
PYTHIA_LDFLAGS	= -L$(shell pythia8-config --libdir) -lpythia8 -lHepMC
RIVET_CONFIG	= rivet-config
RIVET_CXXFLAGS	= $(shell $(RIVET_CONFIG) --cppflags) \
		  $(shell $(RIVET_CONFIG) --cflags)
RIVET_LDFLAGS	= $(shell $(RIVET_CONFIG) --ldflags) -lHepMC -lYODA -lRivet
CXX		= g++ -c -g 
CXXFLAGS	= 
LD		= g++ -rdynamic -g
LDFLAGS		=  

%.o:	%.cc
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $<

%:%.o
	$(LD) $(LDFLAGS) -o $@ $<

all:	angantyr pythia rivet RivetHepMC2ROOT.so 

RivetHepMC2ROOT.so:	HepMC2ROOT.cc Primaries2ROOT.cc TreeOutput.hh
	rivet-buildplugin -r $@ $(filter %.cc, $^) -g -lEG

clean:
	rm -f angantyr pythia RivetHepMC2ROOT.so rivet
	rm -f *.o
	rm -f *~
	rm -f *.hepmc *.root *.yoda
	rm -f $(DISTDIR).tar.gz
	rm -rf $(DISTDIR)

angantyr.o:	CXXFLAGS=$(PYTHIA_CXXFLAGS)
pythia.o:	CXXFLAGS=$(PYTHIA_CXXFLAGS)
rivet.o:	CXXFLAGS=$(RIVET_CXXFLAGS)

angantyr:	LDFLAGS=$(PYTHIA_LDFLAGS)
pythia:		LDFLAGS=$(PYTHIA_LDFLAGS)
rivet:		LDFLAGS=$(RIVET_LDFLAGS)

angantyr:	angantyr.o 
pythia:		pythia.o   
rivet:		rivet.o    

angantyr.o:	angantyr.cc Progress.hh Option.hh MyPythia.hh
pythia.o:	pythia.cc Progress.hh Option.hh MyPythia.hh
rivet.o:	rivet.cc Option.hh MyRivet.hh

angantyr.cc pythia.cc:	Progress.hh Option.hh MyPythia.hh
rivet.cc:		Option.hh MyRivet.hh

dist:distdir
	tar -czvf $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

distdir:
	mkdir -p $(DISTDIR)
	cp angantyr.cc pythia.cc rivet.cc HepMC2ROOT.cc Primares2ROOT.cc \
	   MyPythia.hh MyRivet.hh Option.hh Progress.hh TreeOutput.hh Makefile \
	   $(DISTDIR)/



test:	pythia rivet RivetHepMC2ROOT.so 
	./pythia -n 1000 | \
		./rivet -i - -L . \
			-a HepMC2ROOT:output=$<.root \
			-v MyRivet=TRACE


testaa:	angantyr rivet RivetHepMC2ROOT.so 
	./angantyr -n 100 | \
		./rivet	-i - -L .
			-a HepMC2ROOT:unique=true:output=$<.root	\
			-a Primaries2ROOT:output=prim.root:source=prim.C

