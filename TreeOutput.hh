/**
 * @file   TreeOutput.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 15 14:00:12 2019
 * @brief  Utility to write particles to a ROOT TTree 
 * @copy   2019 Christian Hom Christensen
 */
#ifndef TREEOUTPUT_HH
#define TREEOUTPUT_HH
#include <Rivet/Tools/Logging.hh>
#include <TTree.h>
#include <TParticle.h>
#include <TClonesArray.h>
#include <TDatabasePDG.h>
#include <TFile.h>
#include <THashList.h>
#include <TMath.h>
#include <map>

namespace Rivet
{
  namespace Tools
  {
    /**
     * Event header information on a separate branch on output.  One
     * can copy this to ones ROOT analysis to easily get the
     * information from the header.
     */
    struct TreeHeader
    {
      double b;          // Impact parameter 
      double psi;        // Event plane angle  
      double epsilon;    // Eccentricity 
      double sigmaNN;    // NN inelastic cross-section
      double sqrtS;      // Collision energy
      double asqrtS;     // Collision energy per nucleon pair
      double weight;     // Event weight                     
      int beam1;         // Beam species 
      int beam2;         // Beam species 
      int ncoll;         // # of collisions 
      int ncoll_hard;    // # of hard collisions 
      int npart_proj;    // # of projectile participants
      int npart_targ;    // # of target participangs 
      int nspec_neut;    // # of neutron spectators 
      int nspec_prot;    // # of proton spectators 
      int ncoll_ww;      // # of wounded-wounded collisions 
      int ncoll_nw;      // # of N-wounded collisions 
      int ncoll_wn;      // # of wounded-N collisions 
      
      /** 
       * Reset values 
       */
      void reset()
      {
	beam1      = 0;
	beam2      = 0;    
	ncoll      = 0;
	ncoll_hard = 0;
	npart_proj = 0;
	npart_targ = 0;
	nspec_neut = 0;
	nspec_prot = 0;
	ncoll_ww   = 0;
	ncoll_nw   = 0;
	ncoll_wn   = 0;
	b          = 0;
	psi        = 0;
	epsilon    = 0;
	sigmaNN    = 0;
	sqrtS      = 0;
	asqrtS     = 0;
	weight     = 0;
      }
      
      /** 
       * Fill values from event 
       * 
       * @param ev Event 
       */
      void fill(const Rivet::Event& ev)
      {
	reset(); 
	
	beam1  = ev.beams().first.pid();
	beam2  = ev.beams().second.pid();
	sqrtS  = ev.sqrtS();
	asqrtS = ev.asqrtS();
	weight = ev.weight();
	
	const HepMC::GenEvent* gev = ev.genEvent();
	if (!gev) return;
	
	const HepMC::HeavyIon* hi = gev->heavy_ion();
	if (!hi || !hi->is_valid()) return;
	
	ncoll      = hi->Ncoll();
	ncoll_hard = hi->Ncoll_hard();
	npart_targ = hi->Npart_targ();
	npart_proj = hi->Npart_proj();
	nspec_neut = hi->spectator_neutrons();
	nspec_prot = hi->spectator_protons();
	ncoll_nw   = hi->N_Nwounded_collisions();
	ncoll_wn   = hi->Nwounded_N_collisions();
	ncoll_ww   = hi->Nwounded_Nwounded_collisions();
	b          = hi->impact_parameter();
	psi        = hi->event_plane_angle();
	epsilon    = hi->eccentricity();
	sigmaNN    = hi->sigma_inel_NN();
      }
      /** 
       * Print content of header to standard output 
       * 
       * @param o Output stream
       */
      void print(std::ostream& o=std::cout) const
      {
	o << "                     Beam species: " << beam1      << "\n"
	  << "                     Beam species: " << beam2      << "\n"
	  << "                  # of collisions: " << ncoll      << "\n"
	  << "             # of hard collisions: " << ncoll_hard << "\n"
	  << "     # of projectile participants: " << npart_proj << "\n"
	  << "         # of target participangs: " << npart_targ << "\n"
	  << "          # of neutron spectators: " << nspec_neut << "\n"
	  << "           # of proton spectators: " << nspec_prot << "\n"
	  << "  # of wounded-wounded collisions: " << ncoll_ww   << "\n"
	  << "        # of N-wounded collisions: " << ncoll_nw   << "\n"
	  << "        # of wounded-N collisions: " << ncoll_wn   << "\n"
	  << "                 Impact parameter: " << b          << " fm\n"
	  << "                Event plane angle: " << psi        << "\n"
	  << "                     Eccentricity: " << epsilon    << "\n"
	  << "       NN inelastic cross-section: " << sigmaNN    << " mb\n"
	  << "                 Collision energy: " << sqrtS      << " GeV\n"
	  << "Collision energy per nucleon pair: " << asqrtS     << " GeV\n"
	  << "                     Event weight: " << weight     << "\n"
	  << std::flush;
      }
      /** 
       * Write our code for header structure used in an analysis
       * 
       * @param o Output stream
       */
      static void saveSource(std::ostream& o)
      {
	o << "struct Header {\n"
          << "  double b;          // Impact parameter\n"
          << "  double psi;        // Event plane angle\n"
          << "  double epsilon;    // Eccentricity\n"
          << "  double sigmaNN;    // NN inelastic cross-section\n"
          << "  double sqrtS;      // Collision energy\n"
          << "  double asqrtS;     // Collision energy per nucleon pair\n"
          << "  double weight;     // Event weight\n"
          << "  int beam1;         // Beam species\n"
          << "  int beam2;         // Beam species\n"
          << "  int ncoll;         // # of collisions\n"
          << "  int ncoll_hard;    // # of hard collisions\n"
          << "  int npart_proj;    // # of projectile participants\n"
          << "  int npart_targ;    // # of target participangs\n"
          << "  int nspec_neut;    // # of neutron spectators\n"
          << "  int nspec_prot;    // # of proton spectators\n"
          << "  int ncoll_ww;      // # of wounded-wounded collisions\n"
          << "  int ncoll_nw;      // # of N-wounded collisions\n"
          << "  int ncoll_wn;      // # of wounded-N collisions\n"
	  << "};" << std::endl;
      }
    };

    /** 
     * Utility to store particles in a ROOT TTree in a TFile 
     *
     * Analyses can derive from this class or contain an object of
     * this class. 
     *
     * Example of using as a base class 
     *
     * @code 
     class PrimariesAndPhis : public Analysis, TreeOutput
     {
     public:
       PrimariesAndPhis() 
         : Analysis("PrimariesAndPhis"), TreeOutput("out.root")
       {}
       void init()
       { 
         setupTree();

         const PrimaryParticle pp;
         declare(pp, "Primaries");
        
         const PhiSelector phis;
         declare(phis, "Phis");
       }
       void analyze(const Event& e)
       { 
         const Particles& prim = 
           apply<PrimaryParticles>(event,"Primary").particles();
         const Particles& phis = 
           apply<PhiSelector>(event,"Phis").particles();
         
         fillTreeHeader(e);
         fillTreeParticles(prim);
         fillTreeParticles(phis);
         fillTree();
       }
       void finalize() 
       {
         closeTree();
       }
     };
     @endcode 
     *
     * See also below 
     *
     * An example of analysing the output of this in ROOT could be 
     *
     *
     @code 
     struct Header
     {
       double b;          // Impact parameter 
       double psi;        // Event plane angle  
       double epsilon;    // Eccentricity 
       double sigmaNN;    // NN inelastic cross-section
       double sqrtS;      // Collision energy
       double asqrtS;     // Collision energy per nucleon pair
       double weight;     // Event weight 
       int beam1;         // Beam species 
       int beam2;         // Beam species 
       int ncoll;         // # of collisions 
       int ncoll_hard;    // # of hard collisions 
       int npart_proj;    // # of projectile participants
       int npart_targ;    // # of target participangs 
       int nspec_neut;    // # of neutron spectators 
       int nspec_prot;    // # of proton spectators 
       int ncoll_ww;      // # of wounded-wounded collisions 
       int ncoll_nw;      // # of N-wounded collisions 
       int ncoll_wn;      // # of wounded-N collisions 
     };
     
     void restorePDGs(TDirectory* dir)
     {
       THashList* l = static_cast<THashList*>(dir->Get("pdgs"));
       if (!l) return;
     
       TIter itr(l);
       TDatabasePDG* db = TDatabasePDG::Instance();
       TParticlePDG* p  = 0;
       while ((p = static_cast<TParticlePDG*>(itr()))) {
         if (db->GetParticle(p->PdgCode())) continue;
     
         db->AddParticle(p->GetName(), p->GetTitle(), p->Mass(),
     		    p->Stable(), p->Width(), p->Charge(),
     		    p->ParticleClass(), p->PdgCode());
       }
     }
       
     void analyse(const char* input="out.root")
     { 
       int           entry     = 0;
       TFile*        file      = TFile::Open(input, "READ");
       TTree*        tree      = static_cast<TTree*>(file->Get("T"));
       TClonesArray* particles = new TClonesArray("TParticle");
       Header        header;
       TProfile*     meanPt    = new TProfile("meanPt",
     					 "#LT#it{p}_{T}#GT vs. "
     					 "#LT#it{N}_{part}#GT",
     					 100, 0, 400);
       restorePDGs(tree->GetCurrentFile());
       
       tree->SetBranchAddress("header",    &header);
       tree->SetBranchAddress("particles", &particles);
       while (tree->GetEntry(entry++) > 0) {
         int npart = header.npart_proj + header.npart_targ;
     
         TIter next(particles);
         TParticle* p = 0;
         Double_t   sum = 0;
         Int_t      n = 0;
         while ((p = static_cast<TParticle*>(next()))) {
           if (!p->GetPDG() || p->GetPDG()->Charge() == 0) continue;
           sum += p->Pt();
           n++;
         }
         meanPt->Fill(npart, sum / n, header.weight);
       }
       meanPt->Draw();
     }
     @endcode 
     */
    class TreeOutput
    {
    public:
      /** 
       * Constructor 
       * 
       * @param outfilename Output file name 
       */
      TreeOutput()
	: _tree(0),
	  _particles(0),
	  _header()
      {}
      /** 
       * Destructor.  Calls close to close output if not done already 
       */
      virtual ~TreeOutput()
      {
	closeTree();
      }
      /**
       * Add a particle to the PDG database 
       *
       * @param name Name of particle 
       * @param mass Mass (in GeV) of particle 
       * @param stable If true, particle is considered stable
       * @param width Decay width (in GeV) of particle 
       * @param charge Electric charge (in 1/3e) of particle 
       * @param cls Particle class 
       * @param code PDG code of particle 
       * @param anti If true, register anti-particle 
       */
      void addParticle(const char* name,
		       double mass,
		       bool stable,
		       double width,
		       double charge,
		       const char* cls,
		       int code,
		       bool anti=false)
      {
	TDatabasePDG* db = TDatabasePDG::Instance();
	if (!db->GetParticle(code)) 
	  db->AddParticle(name,name,mass,stable,width,charge,cls,code);
	if (anti && !db->GetParticle(-code))
	  db->AddAntiParticle(("Anti" + std::string(name)).c_str(), -code);
      }
      /** 
       * Construct and ion PDG code 
       * 
       * @param z      Atomic number 
       * @param a      Atomic weight  
       * @param isomer Isomer number 
       * 
       * @return PDG code 
       */
      int ionPdg(size_t z, size_t a, size_t isomer=0) const
      {
	return (1000000000 + /* ION identifier   */
		0*10000000 + /* # strange quarks */
		z*10000    + /* atomic number    */
		a*10       + /* atomic weight    */
		isomer*1);   /* Isomer number    */
      }
      /** 
       * Add an ion to the PDG database 
       * 
       * @param z       Atomic number 
       * @param a       Atomic weight 
       * @param width   Possible decay width
       * @param isomer  Isomer number 
       */
      void addIon(size_t z,
		  size_t a,
		  double width=0,
		  size_t isomer=0)
      {
	int pdg = ionPdg(z,a,isomer);
	TDatabasePDG* db = TDatabasePDG::Instance();
	if (db->GetParticle(pdg)) {
	  MSG_TRACE("Already has ion PDG " << pdg); 
	  return;
	}
	// Periodic table to autogenerate ion names 
	const char* names[] =
	  { 
	   "H",	                                                                                                                                                      "He",
	   "Li","Be",                                                                                                                        "B", "C", "N", "O", "F", "Ne",
	   "Na","Mg",                                                                                                                        "Al","Si","P", "S", "Cl","Ar",
	   "K", "Ca","Sc",                                                                      "Ti","V", "Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr",
	   "Rb","Sr","Y",                                                                       "Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I", "Xe",
	   "Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W", "Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn",
	   "Fr","Ra","Ac","Th","Pa","U", "Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs","Mt","Ds","Rg","Cn","Nh","Fl","Mc","Lv","Ts","Og" };
	std::string name(z > 118 ? "XX" : names[z-1]);
	name += std::to_string(a);
	if (isomer > 0) name += "(" + std::to_string(isomer) + ")";

	MSG_DEBUG("Adding " << name << " ion (" << pdg << ")");
	const double pm = TDatabasePDG::Instance()->GetParticle(2212)->Mass();
	const double nm = TDatabasePDG::Instance()->GetParticle(2112)->Mass();
	double       mass = (a-z) * nm + z * pm;
	bool         stable = width <= 1e-18;
	addParticle(name.c_str(),mass,stable,width,3*z,"ION",pdg);
      }		  
      /**
       * Register additional PDGs 
       */
      void setupPDGs()
      {
	const char*  spe  = "Special";
	const char*  ion  = "Ion";
	const char*  res  = "Resonance";
	const char*  mes  = "Meson";
	const double kE2G = 1e7 / 1.6021773349e-3;
	const double kY2W = TMath::Hbar() * kE2G / (3600*24*365.24);

	// pomeron, odderon, reggeron as defined by PDG 
	addParticle("pomeron",0,false,0,0,"GaugeBoson",990);
	addParticle("odderon",0,false,0,0,"GaugeBoson",9990);
	addParticle("reggeon",0,false,0,0,"GaugeBoson",110);
	
	// Bottom mesons
	addParticle("Upsilon(3S)",10.3552,true,0,1,"Bottonium",200553);

	// QCD diffractive states
	addParticle("rho_diff0",0,true,0,0,"QCD diffr. state",9900110);
	addParticle("pi_diffr+",0,true,0,1,"QCD diffr. state",9900210);
	addParticle("omega_di", 0,true,0,0,"QCD diffr. state",9900220);
	addParticle("phi_diff", 0,true,0,0,"QCD diffr. state",9900330);
	addParticle("J/psi_di", 0,true,0,0,"QCD diffr. state",9900440);
	addParticle("n_diffr0", 0,true,0,0,"QCD diffr. state",9902110);
	addParticle("p_diffr+", 0,true,0,1,"QCD diffr. state",9902210);

	// From Herwig
	addParticle("PSID",    3.7699,false,0, 0,mes,  30443);
	addParticle("A_00",    0.9960,false,0, 0,mes,9000111);
	addParticle("A_0+",    0.9960,false,0,+3,mes,9000211);
	addParticle("A_0-",    0.9960,false,0,-3,mes,9000211);
	addParticle("KDL_2+",  1.773, false,0,+3,mes,  10325);
	addParticle("KDL_2-",  1.773, false,0,-3,mes, -10325);
	addParticle("KDL_20",  1.773, false,0, 0,mes,  10315);
	addParticle("KDL_2BR0",1.773, false,0, 0,mes, -10315);
	addParticle("PI_2+",   1.670, false,0,+3,mes,  10215);
	addParticle("PI_2-",   1.670, false,0,-3,mes, -10215);
	addParticle("PI_20",   1.670, false,0, 0,mes,  10115);
	addParticle("KD*+",    1.717, false,0,+3,mes,  30323);
	addParticle("KD*-",    1.717, false,0,-3,mes, -30323);
	addParticle("KD*0",    1.717, false,0, 0,mes,  30313);
	addParticle("KDBR*0",  1.717, false,0, 0,mes, -30313);
	addParticle("RHOD+",   1.700, false,0,+3,mes,  30213);
	addParticle("RHOD-",   1.700, false,0,-3,mes, -30213);
	addParticle("RHOD0",   1.700, false,0, 0,mes,  30113);	
	addParticle("ETA_2(L)",1.632, false,0, 0,mes,  10225);
	addParticle("ETA_2(H)",1.854, false,0, 0,mes,  10335);
	addParticle("OMEGA(H)",1.649, false,0, 0,mes,  30223);
	addParticle("KDH_2+",  1.816, false,0,+3,mes,  20325);
	addParticle("KDH_2-",  1.816, false,0,-3,mes, -20325);
	addParticle("KDH_20",  1.816, false,0, 0,mes,  20315);
	addParticle("KDH_2BR0",1.816, false,0, 0,mes, -20315);
	addParticle("KD_3+",   1.773, false,0,+3,mes,    327);
	addParticle("KD_3-",   1.773, false,0,-3,mes,   -327);
	addParticle("KD_30",   1.773, false,0, 0,mes,    317);
	addParticle("KD_3BR0", 1.773, false,0, 0,mes,   -317);
	addParticle("RHO_3+",  1.691, false,0,+3,mes,    217);
	addParticle("RHO_3-",  1.691, false,0,-3,mes,   -217);
	addParticle("RHO_30",  1.691, false,0, 0,mes,    117);
	addParticle("OMEGA_3", 1.667, false,0, 0,mes,    227);
	addParticle("PHI_3",   1.854, false,0, 0,mes,    337);
	addParticle("CHI2P_B0",10.232,false,0, 0,mes, 110551);
	addParticle("CHI2P_B1",10.255,false,0, 0,mes, 120553);
	addParticle("CHI2P_B2",10.269,false,0, 0,mes, 100555);
	addParticle("UPSLON4S",10.580,false,0, 0,mes, 300553);


	// IONS
	addParticle("Deuteron", 1.875613,true, 0,         3,ion,1000010020,true);
	addParticle("Triton",   2.80925, false,kY2W/12.33,3,ion,1000010030,true);
	addParticle("HE3",      2.80923, false,0,         6,ion,1000020030,true);
	addParticle("Alpha",    3.727379,true, kY2W/12.33,6,ion,1000020040,true);

	addIon(82,208);	
	addIon(54,129);

	// Special particles
	addParticle("Cherenkov",     0,     false, 0,0,   spe,50000050);
	addParticle("FeedbackPhoton",0,     false, 0,0,   spe,50000051);
	addParticle("Lambda1520",    1.5195,false, 0156,0,res,3124,
		    true);

	//Hyper nuclei and exotica
	const double sml = 2.5e-15;
	addParticle("HyperTriton",         2.99131,false,sml,  3,ion,1010010030,true);
	addParticle("Hyperhydrog4",        3.931,  false,sml,  3,ion,1010010040,true);
	addParticle("Hyperhelium4",        3.929,  false,sml,  6,ion,1010020040,true);
	addParticle("Hyperhelium5",        4.841,  false,sml,  6,ion,1010020050,true);
	addParticle("DoubleHyperhydrogen4",4.106,  false,sml,  6,ion,1020010040,true);    
	addParticle("LambdaNeutron",       2.054,  false,sml,  0,spe,1010000020,true);
	addParticle("Hdibaryon",           2.23,   false,sml,  0,spe,1020000020,true);
	addParticle("LambdaNeutronNeutron",2.99,   false,sml,  0,spe,1010000030,true);
	addParticle("Xi0Proton",           2.248,  false,5e-15,3,ion,1020010020,true);
	addParticle("OmegaProton",         2.592,  false,sml,  0,spe,1030000020,true);
	addParticle("OmegaNeutron",        2.472,  false,0.003,3,spe,1030010020,true);
	addParticle("OmegaOmega",          3.229,  false,sml,  6,spe,1060020020,true);
	addParticle("Lambda1405Proton",    2.295,  false,0.05, 3,spe,1010010021,true);
	addParticle("Lambda1405Lambda1405",2.693,  false,0.05, 0,spe,1020000021,true);


	// Special resonances
	addParticle("f0_980",       0.980, false,0.07, 0, res,9010221);
	addParticle("f2_1270",      1.275, false,0.185,0, res,225);
	addParticle("Xi_Minus_1820",1.8234,false,0.024,-3,res,123314,true);
	addParticle("Xi_0_1820",    1.8234,false,0.024,0, res,123324,true);
	addParticle("ps_2100",      2.100, false,0.040,3, res,9322134,true);
	addParticle("ps_2500",      2.500, false,0.040,3, res,9322136,true);
	addParticle("d*_2380",      2.38,  false,0.070,3, res,900010020,true);

	// Pythia specials 
        addParticle("J/psi[3S1(8)]",      3.29692,false,0,0,spe,9940003);
        addParticle("chi_2c[3S1(8)]",      3.7562,false,0,0,spe,9940005);
        addParticle("chi_0c[3S1(8)]",     3.61475,false,0,0,spe,9940011);
        addParticle("chi_1c[3S1(8)]",     3.71066,false,0,0,spe,9940023);
        addParticle("psi(2S)[3S1(8)]",    3.88611,false,0,0,spe,9940103);
        addParticle("J/psi[1S0(8)]",      3.29692,false,0,0,spe,9941003);
        addParticle("psi(2S)[1S0(8)]",    3.88611,false,0,0,spe,9941103);
        addParticle("J/psi[3PJ(8)]",      3.29692,false,0,0,spe,9942003);
        addParticle("psi(3770)[3PJ(8)]",  3.97315,false,0,0,spe,9942033);
        addParticle("psi(2S)[3PJ(8)]",    3.88611,false,0,0,spe,9942103);
        addParticle("Upsilon[3S1(8)]",     9.6603,false,0,0,spe,9950003);
        addParticle("chi_2b[3S1(8)]",     10.1122,false,0,0,spe,9950005);
        addParticle("chi_0b[3S1(8)]",     10.0594,false,0,0,spe,9950011);
        addParticle("chi_1b[3S1(8)]",     10.0928,false,0,0,spe,9950023);
        addParticle("Upsilon(2S)[3S1(8)]",10.2233,false,0,0,spe,9950103);
        addParticle("Upsilon(3S)[3S1(8)]",10.5552,false,0,0,spe,9950203);
        addParticle("Upsilon[1S0(8)]",     9.6603,false,0,0,spe,9951003);
        addParticle("Upsilon(2S)[1S0(8)]",10.2233,false,0,0,spe,9951103);
        addParticle("Upsilon(3S)[1S0(8)]",10.5552,false,0,0,spe,9951203);
        addParticle("Upsilon[3PJ(8)]",     9.6603,false,0,0,spe,9952003);
        addParticle("Upsilon(2S)[3PJ(8)]",10.2233,false,0,0,spe,9952103);
        addParticle("Upsilon(3S)[3PJ(8)]",10.5552,false,0,0,spe,9952203);
      }
      /** 
       * Write out full list of PDGs to output 
       * 
       */
      void storePDGs()
      {
	TDatabasePDG::Instance()->ParticleList()
	  ->Write("pdgs",TObject::kSingleKey);
      }
      /** 
       * Setup the output tree and containers 
       * 
       */
      void setupTree(const std::string& outname="data.root")
      {
	TFile* file = TFile::Open(outname.c_str(), "RECREATE");
	_tree       = new TTree("T","T");
	_particles  = new TClonesArray("TParticle");
	_tree->Branch("header",&_header,
		      "b/D:"	    //  1: Impact parameter 
                      "psi:"	    //  2: Event plane angle
                      "epsilon:"    //  3: Eccentricity 		     
                      "sigmaNN:"    //  4: NN inelastic cross-section
                      "sqrtS:"	    //  5: Collision energy		     
                      "asqrtS:"	    //  6: Collision energy per nucleon pair
                      "weight:"     //  7: Event weight
		      "beam1/I:"    //  8: Beam species 		     
		      "beam2:"	    //  9: Beam species 		     
                      "ncoll:"	    // 10: # of collisions 		     
                      "ncoll_hard:" // 11: # of hard collisions 	     
                      "npart_proj:" // 12: # of projectile participants     
                      "npart_targ:" // 13: # of target participangs 
		      "nspec_neut:" // 14: # of neutron spectators 	     
                      "nspec_prot:" // 15: # of proton spectators 	     
                      "ncoll_ww:"   // 16: # of wounded-wounded collisions  
                      "ncoll_nw:"   // 17: # of N-wounded collisions
                      "ncoll_wn"    // 18: # of wounded-N collisions
		      );
	_tree->Branch("particles",&_particles);
	_tree->SetDirectory(file);
	_tree->SetAlias("npart","header.npart_proj+header.npart_targ");
	
	setupPDGs();
      }      
      /** 
       * Call at start of analyse to clear internal stuff and copy header
       */
      void fillTreeHeader(const Event& e)
      {
	_header.fill(e);
	_particles->Clear();
	_mapping.clear();
      }
      /**
       * Call for each collection you want to copy to output.  Note,
       * if this is called multiple times and a particle is present in
       * more than one of the passed containers, then that particle
       * will be stored more than once.  Use fillTreeUniqueParticles
       * to prevent this behaviour.
       *
       * @param ps Container of particles
       */
      void fillTreeParticles(const Particles& ps)
      {
	if (ps.size() <= 0) return;
	for (auto& p : ps) _fill(p);
      }
      /** 
       * Call for each collection you want to copy to output.
       * Particles will be stored only once.
       * 
       * @param ps Container of particles  
       */
      void fillTreeUniqueParticles(const Particles& ps)
      {
	if (ps.size() <= 0) return;
	for (auto& p : ps) _fillUnique(p);
      }      
      /** 
       * Call at end of analyze to fill output tree 
       */
      void fillTree()
      {
	_tree->Fill();
      }
      /** 
       * Call at end of analyze to fill output tree.  Barcodes of
       * daughters and mothers are replaced with corresponding indexes
       * in to the array of particles stored on the output branch, so
       * that one can do a faster look-up of daughters and mothers.
       * Note, ROOT's TParticle only allows storing 2 daughters and
       * mothers.  Note, if a daughter or mother is not stored on the
       * output, the corresponding field will be -1.
       */
      void fillTreeReordered()
      {
	TParticle* p = 0;
	TIter      next(_particles);
	while ((p = static_cast<TParticle*>(next()))) {
	  p->SetFirstDaughter (_findEntry(p->GetFirstDaughter()));
	  p->SetLastDaughter  (_findEntry(p->GetLastDaughter()));
	  p->SetFirstMother   (_findEntry(p->GetFirstMother()));
	  p->SetLastMother    (_findEntry(p->GetSecondMother()));
	}
	fillTree();
      }      
      /** 
       * Fill event information into header and selected particles into
       * the tree, and write entry to disk.
       * 
       * @param e Event 
       * @param p Selected particles 
       */
      void fill(const Event& e, const Particles& p)
      {
	fillTreeHeader(e);
	fillTreeParticles(p);
	fillTree();
      }
      /** 
       * Flush tree to disk and close file if not done already 
       * 
       */
      void closeTree()
      {
	if (!_tree) return;

	TFile* file = _tree->GetCurrentFile();
	file->cd();
	
	// We write out the PDGs so we may restore these in an analysis script. 
	storePDGs();
	
	file->Write(); 
	file->Close();


	delete _particles;
	_tree = 0;
	_particles = 0;
      }
      /** 
       * Hook to execute on each particle. Default implementation does
       * nothing. Users can override this to do more if needed - e.g.,
       * set flags on the ROOT TParticle object or such.
       *
       * @param p  Input Rivet::Particle 
       * @param rp Output ROOT TParticle 
       */
      virtual void hook(const Rivet::Particle& p, TParticle& rp) {}
      /** 
       * Save skeleton analysis code to stream 
       * 
       * @param o Output stream
       */
      static void saveSource(std::ostream& o)
      {
	TreeHeader::saveSource(o);
	o << "void restorePDGs(TDirectory* dir) {\n"
	  << "  THashList* l = static_cast<THashList*>(dir->Get(\"pdgs\"));\n"
	  << "  if (!l) return;\n"
	  << "  TIter itr(l);\n"
	  << "  TParticlePDG* p = 0;\n"
	  << "  TDatabasePDG* db = TDatabasePDG::Instance();\n"
	  << "  while ((p = static_cast<TParticlePDG*>(itr()))) {\n"
	  << "    if (db->GetParticle(p->PdgCode())) continue;\n"
	  << "    db->AddParticle(p->GetName(),p->GetTitle(),p->Mass(),\n"
	  << "                    p->Stable(),p->Width(),p->Charge(),\n"
	  << "                    p->ParticleClass(),p->PdgCode());\n"
	  << "  }\n"
	  << "}\n"
	  << "void analyse(const char* input=\"data.root\") {\n"
	  << "  int  entry = 0;\n"
	  << "  TFile*        file  = TFile::Open(input,\"READ\");\n"
	  << "  TTree*        tree  = static_cast<TTree*>(file->Get(\"T\"));\n"
	  << "  TClonesArray* parts = new TClonesArray(\"TParticle\");\n"
	  << "  Header        head;\n"
	  << "  // Declare histograms, etc. here.\n"
	  << "  restorePDGs(tree->GetCurrentFile());\n"
	  << "  tree->Branch(\"header\",&head);\n"
	  << "  tree->Branch(\"particles\",&parts);\n"
	  << "  while (tree->GetEntry(entry++) > 0) {\n"
	  << "    // Inspect header here\n"
	  << "    TIter itr(parts);\n"
	  << "    TParticle* p = 0;\n"
	  << "    while ((p = static_cast<TParticle*>(itr()))) {\n"
	  << "      // Inspect particle p here\n"
	  << "    }\n"
	  << "  }\n"
	  << "  // Finish analysis here\n"
	  << "}" << std::endl;
      }
    protected:
      /** Output tree */
      TTree* _tree;
      /** Container of particles */
      TClonesArray* _particles;
      /** Header information */
      TreeHeader _header;
      /** Map from barcode to entry number */
      typedef std::map<int,int> Barcode2Entry_t;
      Barcode2Entry_t _mapping;
      
      /** 
       * Get the ID of a particle (the HepMC barcode)
       * 
       * @param p Particle 
       * 
       * @return HepMC barcode or -1 
       */
      int _getId(const Particle& p) const
      {
	const HepMC::GenParticle* gp = p.genParticle();
	if (!gp) return -1;
	return gp->barcode();
      }
      /** 
       * Find entry corresponding to a barcode 
       * 
       * @param barcode Barcode to find 
       * 
       * @return Entry number of barcode, or -1
       */
      int _findEntry(int barcode) const
      {
	Barcode2Entry_t::const_iterator i = _mapping.find(barcode);
	if (i == _mapping.end()) return -1;
	return i->second;
      }
      std::pair<int,int> _firstLast(const Particles& ps)
      {
	if (ps.size() <= 0) return std::make_pair(-1,-1);

	return std::make_pair(_getId(*ps.begin()),
			      _getId(*ps.rbegin()));
      }	
      /** 
       * Fill a particle into container 
       * 
       * @param p Particle 
       */
      void _fill(const Particle& p)
      {
	const HepMC::GenParticle* gp        = p.genParticle();
	const FourVector&         o         = p.origin();
	int                       bc        = _getId(p);
	auto                      mothers   = _firstLast(p.parents());
	auto                      daughters = _firstLast(p.children());

	if (p.pid() > ionPdg(0,0)) 
	  addIon((p.pid() / 10000) % 1000,(p.pid() / 10) % 1000,0,p.pid() % 10);

	int n = _particles->GetEntries();
	TParticle* rp =
	  new ((*_particles)[n]) TParticle(p.pid(),
					   gp ? gp->status() : -1,
					   mothers.first, mothers.second,
					   daughters.first, daughters.second,
					   p.px(), p.py(), p.pz(), p.E(),
					   o.x(), o.y(), o.z(), o.t());
	if (gp) {
	  const HepMC::ThreeVector pol = gp->polarization().normal3d();
	  rp->SetPolarisation(pol.x(),pol.y(),pol.z());
	}
	hook(p,*rp);
	rp->SetUniqueID(bc);
	_mapping[bc] = n;
      } 
      /** 
       * Fill a particle into container, but only if not already seen 
       * 
       * @param p 
       */
      void _fillUnique(const Particle& p)
      {
	if (_mapping.find(_getId(p)) != _mapping.end()) return;

	_fill(p);
      }
      /** 
       * @return Logging handler for this object 
       */
      Log& getLog()
      {
	return Log::getLog("TreeOutput");
      }
    };
  }
}
#endif
//
// EOF
//
