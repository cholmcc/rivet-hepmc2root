/**
 * @file   rivet.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 19 10:51:52 2019
 * @copy   (2019) Christian Holm Christensen
 * @brief  Easy running of Rivet 
 */
#include "MyRivet.hh"

int
main(int argc, char** argv)
{
  MyRivet r;
  return r.run(argc, argv);
}
//
// EOF
//
