/**
 * @file   Progress.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Sat May 27 11:20:45 2017
 * @copyright GNU Lesser Public License.
 * 
 * @brief  A progress meter 
 * 
 * @ingroup alice_rivet_tools_prog
 */
#ifndef RIVET_TOOL_PROGRESS
#define RIVET_TOOL_PROGRESS
#include <chrono>
#include <iostream>
#include <iomanip>
#include <sstream>

//====================================================================
/** 
 * A progress meter 
 *
 */
struct Progress
{
  typedef std::chrono::time_point<std::chrono::system_clock> Time;
  typedef std::chrono::milliseconds Duration;
  /** When we started */
  Time _start;
  /** Current mark */
  Time _mark;
  /** Last lap-time */
    Time _now;
  /** Last write-out */
  Time _last;
  /** Update frequency */
  long _freq;
  /** 
   * Constructor 
   * 
   * @param f Update frequency (in seconds)
   */
  Progress(long f=1) :
    _start(), _mark(), _now(), _freq(f)
  {
    _start = _mark = _now = now();
  }
  /** 
   * Get current time - a convinience function
   * 
   * @return A time-point
   */
  static Time now() { return std::chrono::system_clock::now(); }
  
  /**
   * Store current time 
   */
  void split() { _now = now(); }
  /** 
   * Set mark 
   */
  void mark() { _mark = now(); }
  /** 
   * Get the time (in seconds) since the start
   * 
   * @return Time elapsd in seconds 
   */
  Duration since_start() const {
    return std::chrono::duration_cast<Duration>(_now - _start);
  }
  /** 
   * Get the time (in seconds) since the last mark
   * 
   * @return Time elapsd in seconds 
   */
  Duration since_mark() const {
    return std::chrono::duration_cast<Duration>(_now - _mark);
  }
  /** 
   * Get the time (in seconds) since the last mark
   * 
   * @return Time elapsd in seconds 
   */
  Duration since_last() const {
    return std::chrono::duration_cast<Duration>(_now - _last);
  }
  /** 
   * Calculate how much time remains 
   *
   * @param cur event number 
   * @param end Maximum event number 
   *
   * @return seconds remaning, or -1 if not possible to calculate 
   */
  Duration to_end(long cur, long end)
  {
    if (end  <=  0)    return since_mark();
    if (_now == _mark) return Duration::max();
    if (cur  <=  0)    return Duration::max();
    double f   = double(end-cur) / cur;
    return std::chrono::duration_cast<Duration>(since_mark() * f);
  }
  /**
   * Format a time string 
   *
   * @param sec Duration 
   */
  std::string format(Duration dur, bool wms=false)
  {
    if (dur == Duration::max()) return "?";
    
    auto hour = std::chrono::duration_cast<std::chrono::hours>(dur);
    auto minu = std::chrono::duration_cast<std::chrono::minutes>(dur);
    auto seco = std::chrono::duration_cast<std::chrono::seconds>(dur);
    std::stringstream s;
    s << std::setw(4) << hour.count() << ":"
      << std::setfill('0')
      << std::setw(2) << (minu.count() % 60) << ":"
      << std::setw(2) << (seco.count() % 60);
    if (wms) {
      auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(dur);
      s << "." << std::setw(3) << (ms.count() % 60);
    }
    return s.str();
  }
  /**
   * Print current progress 
   */
  void print(long cur, long end, const char* pre="Processed")
  {
    // If frequency is less than or equalt to 0, never print
    if (_freq == 0) return;
    
    // Take current time
    split();
    if (_freq < 0) {
      if (cur % -_freq != 0) return;
    }
    else {
      auto upd =
	std::chrono::duration_cast<std::chrono::seconds>(since_last());
      if (upd.count() < _freq) return;
    }
    _last = now();
    
    // Get some durations 
    Duration rem = to_end(cur, end);
    Duration spe = to_end(cur, 0);
    Duration ela = since_start();
    auto     ssp = std::chrono::duration_cast<std::chrono::seconds>(spe);
    std::stringstream s;
    s.precision(3);
    
    // Write start of line 
    s << std::left << std::setw(9) << pre << std::right
      << std::setw(9) << cur;
    if (end > 0) s << " of " << std::setw(9) << end;
    
    // Write out times 
    s << " elapsed: " << format(ela) << " (" << std::setw(6);
    
    // write frequency 
    if (ela.count() > 0) s << double(cur)/ssp.count();
    else         s << "?";
    s << "/s)";
    
    // If we have a target number, write the ETA
    if (end > 0) s << " ETA: " << format(rem, true);
    
    std::cout << "\r" << s.str();
    if (cur == end) std::cout << std::endl;
    else            std::cout << std::flush;
  }
  /** 
   * At the end of the job
   * 
   * @param total Total number of events processed 
   */
  void end(long total)
  {
    // print(total, total);
    std::cout << std::endl;
  }
};
#endif
//
// EOF
//

