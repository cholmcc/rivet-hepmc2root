/**
 * @file   Option.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 19 09:09:29 2019
 * @copy   (2019) Christian Holm Christensen
 * @brief  Command line option parseing 
 */
#ifndef OPTION_HH
#define OPTION_HH
#include <string>
#include <iomanip>
#include <iostream>
#include <list>
#include <vector>
#include <iterator>

//__________________________________________________________________
/** 
 * Base class for options 
 */
struct OptionBase
{
  char        _short;
  std::string _long;
  std::string _desc;
  std::string _arg;
  bool        _multi;
  /** 
   * Constructor 
   * 
   * @param s Short option
   * @param l Long option 
   * @param d Descroption 
   * @param a Dummy argument 
   */
  OptionBase(char s,
	     const std::string& l,
	     const std::string& d,
	     const std::string& a,
	     bool               m=false)
    : _short(s),
      _long(l),
      _desc(d),
      _arg(a),
      _multi(m)
  {}
  /** 
   * @return string representation of value
   */
  virtual std::string toString(size_t i=0) const = 0;
  /** 
   * @return true if multi-valued
   */
  virtual bool isMulti() const { return _multi; }
  /** 
   * @return number of values 
   */
  virtual size_t count() const = 0;
  /** 
   * Print information on option 
   * 
   * @param o Output stream 
   */
  virtual void help(std::ostream& o,
		    size_t nl=11,
		    size_t na=11,
		    size_t nd=40) const
  {
    o << "  -" << _short                               //3+1=4
      << ",--" << std::left << std::setw(nl) << _long  //3+11=14
      << " "   << std::left << std::setw(na) << _arg   //1+11=12
      << " "   << std::left << std::setw(nd) << _desc;
  }
  virtual void print(std::ostream& o,size_t nd=40) const = 0;
  /** 
   * @return true if option takes argument 
   */
  virtual bool hasArg() const { return !_arg.empty(); }
  /** 
   * Set value 
   * 
   * @param v Value (as string)
   */
  virtual void set(const char* v) = 0;
  /** 
   * Set value 
   * 
   * @param v Value (as string)
   */
  virtual void set(const std::string& v) = 0;
  /** 
   * Toggle value
   */
  virtual void toggle() {}
  /** 
   * Check if this option accepts this argument 
   * 
   * @param s Short option 
   * @param v Value 
   * 
   * @return true if taken 
   */
  virtual bool accept(char s, const char* v)
  {
    if (s != _short) return false;
    if (hasArg())    set(v);
    else             toggle();
    return true;
  }
  /** 
   * Check if this option accepts this argument 
   * 
   * @param l Long option 
   * @param v Value 
   * 
   * @return true if taken 
   */
  virtual bool accept(const char* l, const char* v)
  {
    if (l != _long) return false;
    if (hasArg())   set(v);
    else            toggle();
    return true;
  }
};
//__________________________________________________________________
namespace std
{
  std::string to_string(const std::string& s) { return s; }
}

//__________________________________________________________________
/** 
 * Option template
 */
template <typename T>
struct Option : public OptionBase
{
  /** Value */
  using Values = std::vector<T>;
  Values _values;
  /** 
   * Constructor.  This option will be singled valued 
   * 
   * @param s Short option 
   * @param l Long option 
   * @param d Description 
   * @param a Dummy argument 
   * @param v Starting value 
   */
  Option(char s,
	 const std::string& l,
	 const std::string& d,
	 const std::string& a,
	 const T&           v)
    : OptionBase(s, l, d, a, false), _values()
  {
    _values.push_back(v);
  }
  /** 
   * Constructor.  This option will be multi-values
   * 
   * @param s  Short option
   * @param l  Long option
   * @param d  Description 
   * @param a  Dummy argument 
   * @param vs Initial values 
   */
  Option(char s,
	 const std::string& l,
	 const std::string& d,
	 const std::string& a,
	 const std::initializer_list<T> vs)
    : OptionBase(s,l,d,a,true), _values(vs.begin(),vs.end())
  {}
  /** 
   * Set value using a string stream 
   * 
   * @param v Value
   */
  void set(const char* v)
  {
    set(std::string(v));
  }
  void set(const std::string& v)
  {
    std::stringstream s(v);
    T vv;
    s >> vv;
    if (_multi) _values.push_back(vv);
    else        _values[0] = vv;
  }    
  /** 
   * @return Number of values stored
   */
  size_t count() const { return _values.size(); }
  /** 
   * Do nothing 
   */
  void toggle() {}
  /** 
   * @return String representation of value 
   */
  std::string toString(size_t i=0) const
  {
    if (!_multi) return std::to_string(_values[0]);
    return std::to_string(_values[i]);
  }
  /**
   * Get a value 
   */
  const T& value(size_t i=0) const { return _values[i]; }
  /** 
   * @return Get container of values 
   */
  const Values& values() const { return _values; }
  /** 
   * Print information on option 
   * 
   * @param o Output stream 
   */
  virtual void help(std::ostream& o,
		    size_t nl=11,
		    size_t na=11,
		    size_t nd=40) const
  {
    OptionBase::help(o,nl,na,nd);
    if (_multi) o << "{";
    std::copy(_values.begin(), _values.end(),
	      std::ostream_iterator<T>(o, ", "));
    if (_multi) o << "}";
    o << std::endl;
  }    
  /** 
   * Print 
   */
  virtual void print(std::ostream& o, size_t nd=40) const
  {
    o << std::left << std::setw(nd) << _desc << ": ";
    std::copy(_values.begin(), _values.end(),
	      std::ostream_iterator<T>(o, " "));
    o << std::endl;
  }
};
//__________________________________________________________________
/** 
 * Specialisation for booleans
 */
template <>
struct Option<bool> : public OptionBase
{
  /** Value */
  bool _value;
  /** 
   * Constructor. Never multi-values
   * 
   * @param s Short option
   * @param l Long option 
   * @param d Description 
   * @param v Value 
   */
  Option(char s,
	 const std::string& l,
	 const std::string& d,
	 const bool&        v)
    : OptionBase(s, l, d, "",false), _value(v)
  {}
  /** 
   * Set value 
   * 
   * @param v String 
   */
  void set(const std::string& v)
  {
    std::stringstream s(v);
    s << std::boolalpha;
    s >> _value;
  }
  void set(const char* v)
  {
    set(std::string(v));
  }
  /** 
   * @return Number of values, always 1
   */
  size_t count() const { return 1; }
  /** 
   * @param i ignored
   * @return  the value 
   */
  bool value(size_t i=0) const { return _value; }
  /** 
   * Toggle value
   */
  void toggle()
  {
    _value = !_value;
  }
  /** 
   * @param i ignored
   * @return String representation 
   */
  std::string toString(size_t i=0) const { return _value ? "true" : "false"; }
  /** 
   * Print 
   */
  virtual void print(std::ostream& o, size_t nd=40) const
  {
    o << std::left << std::setw(nd) << _desc
      << ": " << std::boolalpha << _value << std::endl;
  }
  /** 
   * Print information on option 
   * 
   * @param o Output stream 
   */
  virtual void help(std::ostream& o,
		    size_t nl=11,
		    size_t na=11,
		    size_t nd=40) const
  {
    OptionBase::help(o,nl,na,nd);
    o << std::boolalpha << _value << std::endl;
  }    
};

//____________________________________________________________________
/**
 * Command line option manager 
 * 
 */
struct CommandLine
{
  /**
   * Constructor 
   */
  CommandLine() : _all() {}
  /** 
   * Add an option
   * 
   * @param o Option 
   */
  void add(OptionBase& o)
  {
    _all.push_back(&o);
  }
  /** 
   * Find the widest long option 
   * 
   * @return Widest long option 
   */
  size_t longWidth() const
  {
    size_t r = 0;
    for (auto oo : _all) r = std::max(r,oo->_long.length());
    return r;
  }
  /** 
   * Find the widest dummy argument 
   * 
   * @return Widest dummy argument 
   */
  size_t argWidth() const
  { 
    size_t r = 0;
    for (auto oo : _all) r = std::max(r,oo->_arg.length());
    return r;
  }
  /** 
   * Find the widest description string 
   * 
   * @return Widest description string 
   */
  size_t descWidth() const
  { 
    size_t r = 0;
    for (auto oo : _all) r = std::max(r,oo->_desc.length());
    return r;
  }
  /** 
   * Parse the command line 
   * 
   * @param argc Number of arguments 
   * @param argv Vector of arguments 
   * 
   * @return 
   */
  bool parse(int argc, char** argv)
  {
    for (int i=1; i < argc; i++) {
      bool taken = false;
      bool eat   = false;
      for (auto oo : _all) {
	if (argv[i][0] == '-') {
	  if (argv[i][1] == '-' && oo->accept(&(argv[i][2]), argv[i+1])) {
	    taken = true;
	    eat   = oo->hasArg();
	  }
	  else if (oo->accept(argv[i][1], argv[i+1])) {
	    taken = true;
	    eat   = oo->hasArg();
	  }
	}
	if (taken) break;
      }
      if (eat) i++;
      if (!taken) return false;
    }
    return true;
  }
  /** 
   * Show usage 
   * 
   * @param prog Program name 
   * @param o    Output stream 
   */
  void usage(const char* prog, std::ostream& o=std::cout)
  {
    o << "Usage: " << prog << " [OPTIONS]\n\n"
      << "Options:\n";
    size_t nl = longWidth()+1;
    size_t na = argWidth()+1;
    size_t nd = descWidth()+1;
    for (auto oo : _all) oo->help(o,nl,na,nd);
  }
  /** Show values */
  void print(std::ostream& o=std::cout)
  {
    size_t nd = descWidth();
    for (auto oo : _all) oo->print(o,nd);
  }    
  typedef std::list<OptionBase*> Options;
  Options _all;
};
#endif
#ifdef TEST
int main(int argc, char** argv)
{
  CommandLine cl;
  Option<bool>        help   ('h',"help",  "Show help",false);
  Option<int>         integer('i',"int",   "N", "Integer",  42);
  Option<double>      real   ('r',"real",  "X", "Real",     3.14);
  Option<std::string> str    ('s',"string","S", "String",   {});

  cl.add(help);
  cl.add(integer);
  cl.add(real);
  cl.add(str);

  if (!cl.parse(argc, argv) || help._value) {
    cl.usage(argv[0], std::cout);
    return 0;
  }
  cl.print(std::cout);

  return 0;
}
#endif
//
// EOF
//

