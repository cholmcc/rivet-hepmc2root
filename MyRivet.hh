/**
 * @file   MyRivet.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 19 08:53:31 2019
 * @copy   (2019) Christian Holm Christensen
 * 
 * @brief  Wrapper of Rivet analysis handler and run for easier debug. 
 */
#ifndef MYRIVET_HH
#define MYRIVET_HH
#include "Progress.hh"
#include "Option.hh"
#include <Rivet/AnalysisHandler.hh>
#include <Rivet/AnalysisLoader.hh>
#include <Rivet/Analysis.hh>
#include <Rivet/Run.hh>
#include <Rivet/Tools/RivetPaths.hh>
#include <HepMC/IO_GenEvent.h>
#include <YODA/WriterYODA.h>
#include <chrono>
#include <thread>


/**
 * Convert string to lower case 
 */
std::string tolower(const std::string& s)
{
  std::string ret(s);
  std::transform(ret.begin(),ret.end(),ret.begin(),[](char c) { return std::tolower(c); });
  return ret;
}

/**
 * Wrapping Rivet analysis handler and run with sane commandline options 
 * 
 */
struct MyRivet
{
  Rivet::AnalysisHandler _handler;
  Rivet::Run _run;
  CommandLine _cl;
  Option<bool>        _help;
  Option<bool>        _list;
  Option<bool>        _ignoreBeams;
  Option<double>      _xsec;
  Option<std::string> _paths;
  Option<std::string> _datas;
  Option<std::string> _anas;
  Option<std::string> _pre;
  Option<std::string> _logs;
  Option<std::string> _input;
  Option<std::string> _output;
  Option<std::string> _mode;
  Option<unsigned>    _timeout;
  Option<long>        _nev;
  Option<unsigned>    _skip;
  Option<unsigned short> _prec;
  Option<unsigned> _freq;
    
  MyRivet()
    : _handler("MyRivet"),
      _run(_handler),
      _cl(),
      _help       ('h',"help",         "Show help",     false),
      _list       ('l',"list",         "List analyses", false),
      _ignoreBeams('I',"ignore-beams", "Ignore beams",  true),
      _xsec       ('x',"cross-section","Cross section",        "MB",    0),
      _paths      ('L',"analysis-path","Plugin path to search","DIRS",  {}),
      _datas      ('D',"data-path",    "Data path to search",  "DIRS",  {}),
      _anas       ('a',"analyses",     "Analyses",             "NAMES", {}),
      _pre        ('p',"load",         "Read data objects",    "FILES", {}),
      _logs       ('v',"log-level",    "Log levels",           "LEVELS",{}),
      _input      ('i',"input",        "Input file",           "FILES", {}),
      _output     ('o',"output",       "Output file",          "FILE",  "-"),
      _timeout    ('t',"timeout",      "Timeout",              "MS",    100),
      _nev        ('n',"events",       "Events to read",       "EVENTS",-1),
      _skip       ('s',"Skip",         "Events to skip",       "EVENTS",0),
      _prec       ('p',"precision",    "Output precision",     "N",     12),
      _freq       ('f',"progress",     "Progress frequency",   "EVENTS",0),
      _mode       ('m',"mode",         "Job mode",             "MODE",  "full")
      
  {
    _cl.add(_help);
    _cl.add(_ignoreBeams);
    _cl.add(_xsec);
    _cl.add(_paths);
    _cl.add(_datas);
    _cl.add(_anas);
    _cl.add(_pre);
    _cl.add(_logs);
    _cl.add(_list);
    _cl.add(_input);
    _cl.add(_output);
    _cl.add(_timeout);
    _cl.add(_nev);
    _cl.add(_skip);
    _cl.add(_prec);
    _cl.add(_freq);
    _cl.add(_mode);
  }
  /** 
   * Parse input arguments 
   * 
   * @param argc Number of arguments 
   * @param argv Vector of arguments 
   * @param out  Possible output stream 
   * 
   * @return true on success
   */
  bool parse(int argc, char** argv, std::ostream& out=std::cout)
  {
    if (!_cl.parse(argc, argv) || _help.value()) {
      _cl.usage(argv[0], std::cout);
      return false;
    }

    // Set log levels already here so we can use it
    // Note, we do it again later to make sure we propagate
    // to analyses - probably not needed though. 
    setLogs();
    if (getLog().isActive(Rivet::Log::DEBUG)) _cl.print(std::cout);
    
    return true;
  }
  /** 
   * Set Log levels from comma-separeted list 
   * 
   * @param names Comma-separated list of log levels 
   */
  void setLogs()
  {

    for (auto l : _logs.values()) {
      std::stringstream s(l);
      std::string name;
      while (std::getline(s, name, ',')) {
	size_t eq = name.find('=');
	if (eq == std::string::npos) continue;
      
	std::string comp = name.substr(0,eq);
	std::string lvl  = name.substr(eq+1,std::string::npos);
	std::transform(lvl.begin(), lvl.end(), lvl.begin(), ::toupper);
	Rivet::Log::setLevel(comp, Rivet::Log::getLevelFromName(lvl));
      }
    }
  }
  /** 
   * List available analyses 
   * 
   * @param o Output stream 
   */
  void listAvailable(std::ostream& o)
  {
    typedef std::vector<std::unique_ptr<Rivet::Analysis>> List;
    List all = Rivet::AnalysisLoader::getAllAnalyses();
    for (List::const_iterator i = all.begin(); i != all.end(); ++i) {
      std::stringstream s;
      s << std::left << std::setw(25) << (*i)->name() << "  "
	<< std::setw(12) << (*i)->status() << " "
	<< (*i)->summary();
      o << s.str() << std::endl;
    }
  }
  /** 
   * Initialize the job 
   * 
   * @return true on sucess 
   */
  bool init()
  {
    _handler.setIgnoreBeams(_ignoreBeams.value());
    _run.setListAnalyses(_list.value());
    if (_xsec.value() > 0) _handler.setCrossSection(_xsec.value());

    takeValues(_paths.values(),Rivet::addAnalysisLibPath);
    takeValues(_datas.values(),Rivet::addAnalysisDataPath);
    takeValues(_anas.values(),
	       [this] (const std::string& s) { _handler.addAnalysis(s);},',');
    takeValues(_pre.values(),
	       [this] (const std::string& s) { _handler.readData(s); },',');
    setLogs();

    if (_list.value() && _anas.count() <= 0) {
      listAvailable(std::cout);
      return false;
    }
    return true;
  }
  /** 
   * Read a single event, optionally with a timeout.  Try roughly
   * three times within the alloted duration to read an event.  If
   * that fails, we return false.
   * 
   * @param timeout If larger than zero, set this timeout (in
   * miliseconds).
   * 
   * @return true on success 
   */
  bool readEvent(int timeout)
  {
    using Rivet::Log;
    using std::endl;
    
    if (timeout <= 0) return _run.readEvent();
    
    using namespace std::chrono;
    auto start = steady_clock::now();
    do {
      bool ret = _run.readEvent();
      if (ret) return true;
      
      int wt = int(timeout/3+.5);
      MSG_TRACE("Failed to read event, waiting "  << wt << " miliseconds");
      std::this_thread::sleep_for(milliseconds(wt));

      auto now = steady_clock::now();
      auto dur = duration_cast<milliseconds>(now-start).count();

      if (dur > timeout) {
	MSG_TRACE("Event reading timed out after " << dur << " miliseconds");
	break;
      }
    } while (true);
    return false;
  }

  /** 
   * Loop over inputs and events in each input.
   * 
   * @return True on success 
   */
  bool loop()
  {
    using Rivet::Log;
    using std::endl;

    Progress progress(_freq.value());
    long cnt  = 0;
    long nev  = _nev.value();
    long skip = _skip.value();

    std::list<std::string> inputs;
    for (auto l : _input.values()) {
      std::stringstream s(l);
      std::string i;
      while (std::getline(s,i,',')) inputs.push_back(l);
    }

    bool first = true;
    for (auto input : inputs) {
      if (first) {
	MSG_TRACE("Initializing with input \"" << input << "\"");
	_run.init(input, 1);
	first = false;
      }
      else {
	MSG_TRACE("Opening input \"" << input << "\"");
	_run.openFile(input, 1);
	if (!readEvent(_timeout.value())) {
	  MSG_WARNING("Failed to read first event from \"" << input << "\"");
	  continue;
	}
      }

      while (nev < 0 || (cnt - skip) < nev) {
	cnt++;

	if (cnt < skip) {
	  _run.skipEvent();
	  MSG_DEBUG("Skipping event # " << cnt);
	  continue;
	}
	
	if (!_run.processEvent()) {
	  MSG_WARNING("Failed to process event " << cnt
		      << " from \"" << input << "\""); 
	  break;
	}
	progress.print(cnt,nev,"Processing");
	MSG_TRACE("Processed event # " << cnt); 

	if (!readEvent(_timeout.value())) {
	  MSG_DEBUG("No more events in \"" << input << "\"");
	  break;
	}
      }
    }
    progress.print(cnt,nev,"Finished");
    progress.end(nev);
    return true;
  }
  /** 
   * Write out analysis objects 
   * 
   * @return true on success
   */
  bool write(std::ostream& out=std::cout)
  {
    std::string output = _output.value();
    
    const std::vector<Rivet::AnalysisObjectPtr> aos = _handler.getData();
    if (aos.size() <= 0) return true;
    YODA::Writer& w = YODA::WriterYODA::create();
    w.setPrecision(_prec.value());

    try {
      // Note, if output is set to "-" we use our local copy of
      // std::cout which we made _before_ redirecting standard
      // output to std::clog.
      if (output == "-") w.write(out,    aos.begin(), aos.end());
      else               w.write(output, aos.begin(), aos.end());
    } catch (...) {
      throw Rivet::UserError("Unexcepted error in writing file to: " + output);
      return false;
    }
    return true;
  }    
  /** 
   * Finalize the analysis. 
   * 
   * @param out 
   * 
   * @return 
   */
  bool end(std::ostream& out=std::cout)
  {
    _run.finalize();
    return write(out);
  }
  /** 
   * Run the analysis job 
   * 
   * @param argc Number of input arguments 
   * @param argv Vector of input arguments 
   * @param out Output stream 
   * 
   * @return Status 
   */
  int run(int argc, char** argv, std::ostream& out=std::cout)
  {
    using std::endl;
    using Rivet::Log;
    
    std::cin.tie(0); // Not tied to anything
    out.tie(0);
    
    if (!parse(argc, argv)) {
      MSG_WARNING("Command line parsing failed");
      return 1;
    }
    if (!init()) {
      MSG_TRACE("Stopping after init");
      return 0;
    }

    std::string mode = tolower(_mode.value());
    bool run = mode == "full" || mode == "run";
    bool fin = mode == "full" || mode == "finalize";
    MSG_TRACE("Mode is " << mode);
    
    if (run) {
      if (_input.count() <= 0) {
	MSG_WARNING("No inputs specified in mode " << mode);
	return 1;
      }
      if (!loop()) {
	MSG_WARNING("Event loop failed");
	return 1;
      }
    }
    if (!fin) {
      if (!write(out)) {
	MSG_WARNING("Writing AOs failed");
	return 1;
      }
    }
    else
      if (!end(out)) {
	MSG_WARNING("Finalisation failed");
	return 1;
      }

    return 0;
  }
  /** 
   * Handle some lists of values from option by some function. 
   * 
   * @param vs  List of values 
   * @param f   Function to handle values
   * @param sep Possible separate for each value list
   */
  template <typename F>
  void takeValues(const Option<std::string>::Values& vs,F f,char sep=':')
  {
    using std::endl;
    using Rivet::Log;
    
    for (auto v : vs) {
      MSG_TRACE("Got value(s) " << v << " with sep" << sep);
      std::stringstream s(v);
      std::string name;
      while (std::getline(s, name, sep)) {
	MSG_TRACE("Take value " << name);
	f(name);
      }
    }
  }
  /** 
   * @return Logging handler for this object 
   */
  Rivet::Log& getLog()
  {
    return Rivet::Log::getLog("MyRivet");
  }
};
#endif 
//
// EOF
//

    
