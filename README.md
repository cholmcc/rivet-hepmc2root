# Use Rivet to write ROOT TTree of selected particles


## Content

- README.md This file 
- `Option.hh` 
  Command line option handling 
- `Progress.hh` 
  Progress meter with ETA, etc. 
- `MyPythia.hh` 
  A wrapper around **Pythia** for easy set-up
- `MyRivet.hh` 
  A wrapper around **Rivet** handler and run for easy set-up
- `TreeOutput.hh` 
  A utility class that fill a **ROOT** `TTree` with header and selected
  particle information. 
- `pythia.cc` 
  Run **Pythia** for pp collisions 
- `angantyr.cc`
  Run **Pythia**/**Angantyr** for AA (and pA) collisions 
- `rivet.cc` 
  Easy set-up of **Rivet** analysis 
- `analyse.C` 
  Example of analysing the output for `TreeOutput` 
- `HepMC2ROOT.cc`
  A **Rivet** analysis that writes all particles to a `TTree` 
- `Primaries2ROOT.cc` 
  A **Rivet** analysis that writes all (*ALICE*) primaries to a `TTree` 
- `Makefile` 
  A file for **Make**
